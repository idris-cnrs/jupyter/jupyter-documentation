# Changelog

## 2024-08-13

### JupyterHub

- JupyterHub has been upgraded to the version 5.1.0.
- Users will be able to launch JupyterLab instances in "commun" spaces of `WORK`, `SCRATCH` and `STORE`.

### JupyterLab

- JupyterLab has been upgraded to the version 4.2.4.
- Code server has been upgraded which ships latest VSCode 1.91.
- [`jupytext`](https://github.com/mwouts/jupytext) and
  [`spell_checker`](https://github.com/jupyterlab-contrib/spellchecker) have been added to JupyterLab single user environment.

## 2024-05-07

### JupyterHub

- JupyterHub has been upgraded to the version 4.1.5.
- Users will not be able to modify environment variables `PATH` and `PYTHONPATH` anymore in their JupyterLab instances. Any attempts to mutate them using spawner form will be ignored. More details on why this has been done can be found in [JupyterHub docs](https://jupyterhub.readthedocs.io/en/stable/explanation/websecurity.html).
- Rate limiting has been added to JupyterHub in order to prevent users launching server instances in bulk.

### JupyterLab

- JupyterLab has been upgraded to the version 4.1.6 and Python has been upgraded to 3.11. JupyterLab 4 is a major release with many breaking changes and this have an effect on UI and might have an effect on UX. Please report any bugs or incosistencies.
- JupyterLab has been configured with Language servers for Python, bash, JSON, YAML, Typescript, Javascript, CSS and HTML to have better code editing experience
- Unmaintained community JupyterLab extensions [jupyterlab-quickopen](https://github.com/parente/jupyterlab-quickopen) and [jupyterlab-drawio](https://github.com/QuantStack/jupyterlab-drawio) have been removed from the user environment. We will try to add them back ASAP.
- Code server has been upgraded which ships latest VSCode `1.88`.
- Nerfstudio viewer has been upgraded to use the modern viewer which is based on [viser](https://github.com/nerfstudio-project/viser).

## 2023-12-12

- JupyterHub documentation has been improved and migrated to make it accessible outside
  of firewall. [New documentation](https://idris-cnrs.gitlab.io/jupyter/jupyter-documentation/) is
  based on [Docusaurus](https://docusaurus.io/) and it is hosted on GitLab pages so
  that the documentation is available without any firewall restrictions.
- CNRS logos have been updated.

## 2023-10-10

### JupyterHub

- Spawner form has been reworked to have a more \"reactive\"
  experience. Consult updated section in
  [JupyterHub spawner options](./jupyterhub/spawners#jupyterhub-spawner-options).
- Now users can launch applications like VSCode, Tensorboard, _etc.,_
  directly from JupyterHub without having to pass by JupyterLab. Check
  the new \"Frontend\" option in the spawner form.
- The home page of the JupyterHub has been improved to show more
  details on running servers.
- A new Playground environment has been added to JupyterHub that is
  based on
  [jupyterlite](https://jupyterlite.readthedocs.io/en/stable/). More
  info in docs at [Playground](./getting-started/playground).

### JupyterLab

- A new sandbox feature has been added to launch applications in a
  secure environment. Please check the documentation at
  [Sandbox mode](./jupyterlab/jupyterlab#sandbox-mode).
- CPU and GPU power usage indicators have been added along with
  _realtime_ CO<sub>2</sub>
  emissions. More details on how they are estimated in
  [Power Usage](./jupyterlab/jupyterlab#power-usage-and-co2-emissions-power-usage).

## 2023-07-10

### JupyterHub

- JupyterHub has been upgraded to 4.0.1.
- Spawner form has been improved and more sanity checks have been
  added.
- Now users can launch JupyterLab servers on `visu` partition as well.
  Users need to select \"Visualisation\" in partition dropdown in
  Spawner form.
- Users that have custom shells configured, please check
  [usage of shells](./tips/tips#usage-of-shells).

### JupyterLab

- JupyterLab is upgraded to 3.6.5. Next upgrade will be to JupyterLab
  4 which has a lot of [breaking
  changes.](https://jupyterlab.readthedocs.io/en/stable/getting_started/changelog.html)
- Code Server now runs on a unix socket, where socket file is owned
  exclusively by the user. Hence, additional authentication on the
  Code Server has been removed.
- [Cylc UI](https://github.com/cylc/cylc-ui) application has been
  added.
- [noVNC](https://novnc.com/info.html) application has been added to
  access the remote desktop directly from the browser using
  JupyterLab.
- Application \"Instances\" have been renamed to \"Endpoints\". More
  details in the [Applications](./jupyterlab/jupyterlab#applications).
