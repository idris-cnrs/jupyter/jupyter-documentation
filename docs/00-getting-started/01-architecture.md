import CaptionWrapper from "@site/src/components/CaptionWrapper";
import Image from '@theme/IdealImage';
import architecture from '../../static/img/docs/architecture.png'

# Architecture

The following figure shows the high-level architecture of JupyterHub on
Jean Zay platform.

<CaptionWrapper caption="JupyterHub Architecture on Jean Zay">
<Image img={architecture} alt="architecture"/>
</CaptionWrapper>

The main take-away of this architecture is that every JupyterLab
instances are either launched by SSH\'ing\' into a dedicated frontal
node or on a compute node _via_ a SLURM job. Just before spawning a
JupyterLab instance, the user will be presented with a form to choose
between frontal node (SSH spawner) or compute node (SLURM spawner). In
addition, if user chose to use a compute node, it is possible to
configure the SLURM job details like number of nodes, number of GPUs,
_etc_,. The main job step of this job will be JupyterLab instance and
this it will be submitted to SLURM scheduler. Once JupyterLab instance
starts running, the configurable HTTP proxy will add the JupyterLab
routes and serve them back to the user.
