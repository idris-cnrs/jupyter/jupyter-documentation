import CaptionWrapper from "@site/src/components/CaptionWrapper";
import Image from '@theme/IdealImage';

import hublogin from '../../static/img/docs/hublogin.png'
import ssologin from '../../static/img/docs/ssologin.png'
import hubhome from '../../static/img/docs/hubhome.png'
import ssh_hubspawner from '../../static/img/docs/ssh_hubspawner.png'
import slurm_hubspawner from '../../static/img/docs/slurm_hubspawner.png'
import slurm_hubspawner_opts from '../../static/img/docs/slurm_hubspawner_opts.png'
import hubspawning from '../../static/img/docs/hubspawning.png'

# Using JupyterHub on Jean Zay

## JupyterHub login page

JupyterHub is accessible at
[https://jupyterhub.idris.fr](https://jupyterhub.idris.fr). The following login
page will be presented to the user

<CaptionWrapper caption="Login page of JupyterHub service on Jean Zay platform">
<Image img={hublogin} alt="hub login page"/>
</CaptionWrapper>

To sign-in, the user is invited to click on "Sign in with Jean Zay SSO" button. Once clicked, the user is redirected to the authentication SSO portal.

<CaptionWrapper caption="Login page of JupyterHub service on Jean Zay platform">
<Image img={ssologin} alt="Jean Zay SSO login page"/>
</CaptionWrapper>

The user can access the service by using the same login credentials as
the ones used to access Jean Zay login nodes _via_ SSH session.

:::note

The users can access JupyterHub service only from the IP addresses
declared in their accounts. If the user tries to access the service from
outside of these declared IP addresses, login will be denied.

:::

## JupyterHub user home

Once successfully logged in with Jean Zay SSO, the user is redirected to Jupyterhub homepage.

<CaptionWrapper caption="User home page of JupyterHub service on Jean Zay platform">
<Image img={hubhome} alt="hub home page"/>
</CaptionWrapper>

:::important

Previously, users could only spawn JupyterLab instances _via_
JupyterHub. Now, users can spawn other servers like Tensorboard, MLFlow,
_etc.,_ besides JupyterLab. Thus, there have been some major changes in
the UI of the JupyterHub Home page.

Every server will get a mascot in form of a random emoji which will be
displayed next to the URL. It does not have any significance and it is
rather a aesthetic addition.

:::

This is where users can manage their server instances like JupyterLab,
Tensorboard, MLFlow, _etc_. Each user will be allowed to run 10 server
instances at a given time. Users can name their server instances for
better identification. If users do not wish to name their JupyterLab
instances, they can directly click on `Add New Server Instance` where
JupyterHub will automatically name the servers as `jupyter_0`,
`jupyter_1`, \...

The list of active and inactive server instances will be shown in the
main area of home page. The column \"Frontend\" shows the type of server
running, for instance, in the above figure instance `jupyter_0` running
a VSCode server on a login node. It can be accessed by clicking on the
URL shown in the table. If the user wants to stop this server instance,
it is enough to click on `cancel` button which will terminate the
server. Similarly, instance `jupyter_1` is running on SLURM node with
Tensorboard as its frontend. Node type can be either SLURM or Login and
it tells the user if the server is running on a SLURM node or Login
node. More details on the Node type are provided in the next section.
Rest of the columns are self descriptive.

## JupyterHub spawner options

Once the user clicks `start` button on any inactive instance or start a
new instance, a spawner form will be rendered. A spawner is the one that
starts a single user jupyter servers which can be JupyterLab, VSCode,
Tensorboard, _etc_. Based on the user\'s selection in the spawner form,
appropriate server will be spawned and user will be redirected to it.

Currently, users can spawn servers on two different types of nodes
namely, Login and SLURM. These options are clearly shown in the dropdown
where selecting \"Spawn a server on login (frontal) node\" will spawn
the selected server on a \"specialized\" login node and selecting
\"Spawn a server on SLURM node\" will spawn the given server on a SLURM
node.

:::important

This \"specialized\" login node that is dedicated to JupyterHub and
users cannot access this node by regular `ssh` service. In the previous
version, this \"specialized\" login node is named as \"Interactive\". We
believe this name is not very appropriate and hence, we renamed it to
\"Login\". This helps users to differentiate easily if they are running
the servers on SLURM node or login node.

:::

By selecting \"Spawn a server on login (frontal) node\", following form
will be rendered for the user to configure the server instance:

<CaptionWrapper caption="Spawner options for the login node">
<Image img={ssh_hubspawner} alt="ssh spawner"/>
</CaptionWrapper>

:::warning

As stated on the form, users will have only 1 CPU and 5 GB of memory as
a global quota on Login node. So, avoid spawning too many servers or
using compute/memory intensive applications on Login node.

:::

Users can choose which type of server to spawn by selecting the one of
the options in \"Frontend\" dropdown. In the Login node, users can spawn
either a JupyterLab instance or a VSCode server instance.

To know more about sandbox mode, please consult [Sandbox mode](../jupyterlab/jupyterlab#sandbox-mode).

If the user needs more resources that offered by Login node, they will
need to spawn their servers on Jean Zay SLURM node. By selecting \"Spawn
a server on SLURM node\", following form will be rendered for the user
to configure the server instance:

<CaptionWrapper caption="Spawner options for the slurm node">
<Image img={slurm_hubspawner} alt="slurm spawner"/>
</CaptionWrapper>

Spawning a server on a SLURM node is essentially submitting a SLURM job
with main job step as selected frontend server which can be JupyterLab,
Tensorboard, _etc_.

:::important

If the user do not have any active projects on Jean Zay platform, the
\"Spawn a server on SLURM node\" option will not be available in the
dropdown menu. In those cases, users can only use \"Spawn a server on
login node\"

:::

More advanced SLURM job attributes like number of nodes, partitions,
_etc,._ can be configured by expanding the panel \"Advanced SLURM Job
Configuration Options\".

Most of the fields in the above form are self-explanatory apart from
`Profile` and `Load profile` fields. Users can give a meaningful name to
the current spawn configuration options in the `Profile` input and they
will be saved and made available to the user for the subsequent spawns
in the `Load profile` dropdown. By selecting the profile name from the
`Load profile` dropdown options, the form will be pre-filled with the
saved configuration.

The input `Notebook directory` controls the default root directory of
the Jupyter notebook kernels.

If the user selects Tensorboard as the frontend, a new field
\"TensorBoard logs directory\" will appear and user needs to pass the
path where Tensorboard logs can be found. Similarly, if user selects
MLFlow as the frontend, the field \"MLFlow runs directory\" needs to be
configured.

<CaptionWrapper caption="SLURM job configuration options">
<Image img={slurm_hubspawner_opts} alt="slurm spawner job options"/>
</CaptionWrapper>

Inside the \"Advanced SLURM Job Configuration\", users can use
`Extra SBATCH directives` input to define advanced SLURM job
configuration options that are not provided in the form.

:::important

`Extra SBATCH directives` field is only meant to provide SLURM `#SBATCH`
directives. We noticed the user attempts to pass shell commands like
`conda activate myenv` which will cause `RuntimeError` from now on.

:::

Similarly, users can also define environment variables using
`Environment variables` input.

:::note

Shell expansion IS SUPPORTED in the environment variables input whereas
subshells are not.

:::

## JupyterHub progress page

Once the user configures the spawner, the following page will be
rendered showing the progress of the job.

<CaptionWrapper caption="JupyterLab progress page">
<Image img={hubspawning} alt="hub spawning page"/>
</CaptionWrapper>

:::note

Irrespective of selected server, setting up user environment and
starting a server can take a good minute or two and only after which the
users will be redirected to their instances.

:::

In the case of spawning a server on Login node, launching server
instance involves setting up user environment and then launching the
selected server. In the case of SLURM spawner, we will need to wait
until SLURM controller allocates resources to the job, and once
resources are allocated, user environment is setup, server instance is
started and users will be automatically redirected to their server
instances.

:::note

For servers spawned on Login node, there is no \"real\" job ID.
Therefore, current timestamp is used as the Job ID of the server
instance to have a unique name for the spawner log.

:::

Once selected server instance starts running, the log file of the job
are placed at
`$WORK/jupyterhub_spawner_logs/slurm-spawner-$SLURM_JOB_ID.log` for
SLURM spawner. In the case of servers spawned on Login node, the log
file will be found at
`$WORK/jupyterhub_spawner_logs/login-node-spawner-$JOB_ID.out`.
