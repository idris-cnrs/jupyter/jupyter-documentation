---
id: intro
title: Introduction
slug: /
---

import CaptionWrapper from "@site/src/components/CaptionWrapper";
import Image from '@theme/IdealImage';

import logo from '../static/img/docs/idris-cnrs-genci-logo.png'

# Jupyter Documentation for the Jean Zay HPC Platform

|       |                                                                                                                                                                                                                                 |
| ----- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| CI/CD | [![ci](https://gitlab.com/idris-cnrs/jupyter/jupyter-documentation/badges/main/pipeline.svg)](https://gitlab.com/idris-cnrs/jupyter/jupyter-documentation/-/commits/main)                                                       |
| Docs  | [![docs](https://img.shields.io/badge/docs-passing-green?style=flat&link=https://idris-cnrs.gitlab.io/jupyter/jupyter-documentation/)](https://idris-cnrs.gitlab.io/jupyter/jupyter-documentation/)                             |
| Meta  | [![License](https://img.shields.io/badge/License-Apache_2.0-blue.svg)](https://opensource.org/licenses/Apache-2.0) [![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://prettier.io/) |

<Image img={logo} alt="idris logo"/>

## Beginner users

This is the documentation of Jupyter ecosystem deployed on Jean Zay HPC platform. For
the users who are absolute beginners to the Jupyter ecosystem, we recommend to go
through the basic [terminology](./getting-started/terminology) to understand
different concepts of Jupyter project.

In addition, there is a [playground](./getting-started/playground) available for users
to play around with JupyterLab interface.

:::note

This playground JupyterLab runs entirely in the user's browser and you are **not** using
any resources of Jean Zay. This is a great way to do some simple testing to get
familiarity JupyterLab interface. There are several notebooks provided as examples to
help users onboard.

:::

## Intermediate users

For the users who are already users of JupyterLab/Notebook, we recommend you to start
with [JupyterHub](./jupyterhub/spawners) section on how to spawn different types of
servers using the JupyterHub on Jean Zay.

## Advanced users

If you are already famililar with both JupyterHub and JupyterLab, you can dive into
[JupyterLab](./jupyterlab/jupyterlab) section to check for all the supported
functionalities.

## Connecting from remote server

If you need to access JupyterHub _via_ a remote machine, you will need to do some
additional configuration. Please consult
[Accessing via Remote Server](./tips/remote-access) section on how to do it.
