# Jupyter Documentation for Jean Zay HPC platform

|       |                                                                                                                                                                                                                                 |
| ----- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| CI/CD | [![ci](https://gitlab.com/idris-cnrs/jupyter/jupyter-documentation/badges/main/pipeline.svg)](https://gitlab.com/idris-cnrs/jupyter/jupyter-documentation/-/commits/main)                                                       |
| Docs  | [![docs](https://img.shields.io/badge/docs-passing-green?style=flat&link=https://idris-cnrs.gitlab.io/jupyter/jupyter-documentation/)](https://idris-cnrs.gitlab.io/jupyter/jupyter-documentation/)                             |
| Meta  | [![License](https://img.shields.io/badge/License-Apache_2.0-blue.svg)](https://opensource.org/licenses/Apache-2.0) [![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://prettier.io/) |

This repository contains the source code of the documentation of JupyterHub/JupyterLab
deployment on Jean Zay platform.

This website is built using [Docusaurus 3](https://v3.docusaurus.io/),
a modern static website generator.

## Getting started

In order to build the documentation locally, you need to have `nodejs` installed. It
can be done using `conda` using:

```
conda install -c conda-forge nodejs
```

**Note** that we should installed `nodejs>=18.0` for building documentation.

### Installation

First we need to install all the dependencies before building the documentation.
Assuming that we have already cloned the repository, the following command will install
dependencies

```
$ cd jupyter-documentation
$ npm install
```

### Build

Once all the dependencies are installed, the documentation can be build using following
command

```
$ npm run build
```

This command generates static content into the `build` directory and can be served
using any static contents hosting service.

### Serve

Once all the static assets are produced by the `npm run build` command, the user can
serve the documentation using

```
$ npm run serve
```

This will automatically open the documentation in your default browser.

## Local Development

Please consult the [CONTRIBUTING.md](./CONTRIBUTING.md) for setting up local
development environment.
