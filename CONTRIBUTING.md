# Contributing to Jupyter Documentation

If you're reading this section, you're probably interested in contributing to
documentation. Welcome and thanks for your interest in contributing. There are many
ways to contribute to the project.

## Contribution types

### Bug reports

Bug reports are an important type of contribution - it's important to get feedback
about the documentation and any outdated information. In this fast moving world, it
is almost impossible to keep everything up to date with the limited resources we have.
So, we are very glad if users can report any outdated or misinformation in the
documentation which we will try to correct swiftly.

Besides, if users find any issues with User Interface (UI) and/or User Experience (UX),
we greatly appreciate the feedback. The documentation is based on
[docusaurus](https://docusaurus.io/) a modern static site generator based on React. We
chose it as it offers richer UX and greater flexibility with its plugin ecosystem.

While merge requests fixing bugs are accepted, they are not required - the bug report
in itself is a great contribution.

### Merge requests

If you would like to fix something in the documentation - adding new information,
improvements, typo fixes, bug fixes, _etc_ - merge requests
are welcome!

The most important thing to test the documentation after changes locally before pushing
commits. The next section will brief on how to setup local development environment.

## Development Setup

As already, docusaurus uses React framework for building documentation and hence we need
`nodejs` installed locally. We can do it using `conda`/`mamba`.

### Using a conda environment

It is advisable to work in a virtual environment. You can create a
[conda environment](https://conda.io/docs/user-guide/tasks/manage-environments.html):

```
conda create -n jupyter-docs python nodejs     # Create a conda environment
conda activate jupyter-docs                    # Activate the conda environment
```

Next step is to clone the repository to your local machine and change current directory
to the root of the repository:

```
git clone https://gitlab.com/idris-cnrs/jupyter/jupyter-documentation.git
cd jupyter-documentation
```

Install the dependencies of the documentation:

```
npm install
```

This will install all the package dependencies including the ones required for
development.

### pre-commit

The package uses [pre-commit](https://www.npmjs.com/package/pre-commit) to run styling
packages like
`prettier`, `stylelint`, _etc._. `pre-commit` will be installed as part of development
dependencies in your environment.

Every time user commits any changes with `git commit`, `pre-commit` will run and check
for styling issues. If there are any issues found `git commit` will fail. In that case,
user needs to run `npm run lint` command to fix the styling issues and then attempt to
commit again.

User can check the lint errors at any time using

```
npm run lint:check
```

This will output the potential errors that are blocking the commit.

### Documentation

This project uses [Docusaurus](https://docusaurus.io/) to generate documentation.
n order to make changes to docusaurus and see the changes in real time, use the command

```
npm run start
```

This will open the documentation in your browser and when you make changes to the
sources of docusaurus, they will appear in real time in the browser. If there are any
errors they will be shown in the console where `npm run start` command is executed.
