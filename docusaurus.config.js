// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

import { themes as prismThemes } from 'prism-react-renderer';
require('dotenv').config();

// Define variables for siteUrl and baseUrl so we can use them
const siteUrl = process.env.SITE_URL || 'https://idris-cnrs.gitlab.io/';
const baseUrl = '/jupyter/jupyter-documentation/';
const repoUrl = 'https://gitlab.com/idris-cnrs/jupyter/jupyter-documentation';
const jupyterhubUrl = 'https://jupyterhub.idris.fr/hub/home';
const idrisDocsUrl = 'http://www.idris.fr/jean-zay/';

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'jupyter-documentation',
  tagline:
    'Documentation for JupyterHub/JupyterLab deployment on Jean Zay HPC platform',
  favicon: 'img/favicon.ico',

  // Set the production url of your site here
  url: siteUrl,
  // Set the /<baseUrl>/ pathname under which your site is served
  // For GitHub pages deployment, it is often '/<projectName>/'
  baseUrl: baseUrl,

  // GitHub pages deployment config.
  // If you aren't using GitHub pages, you don't need these.
  // organizationName: 'facebook', // Usually your GitHub org/user name.
  // projectName: 'docusaurus', // Usually your repo name.

  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',

  // Even if you don't use internalization, you can use this field to set useful
  // metadata like html lang. For example, if your site is Chinese, you may want
  // to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: 'en',
    locales: ['en'],
  },

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          routeBasePath: '/',
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
          editUrl: `${repoUrl}/-/tree/main`,
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
          editUrl: `${repoUrl}/-/tree/main`,
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      // image: 'img/gitlab-card.jpg',
      docs: {
        sidebar: {
          // Make sidebar hideable
          hideable: true,
          // Collapse all sibling categories when expanding one category
          autoCollapseCategories: true,
        },
      },
      navbar: {
        title: 'Jean Zay JupyterHub Documentation',
        logo: {
          alt: 'GitLab Logo',
          src: 'img/logo.svg',
        },
        items: [
          {
            type: 'doc',
            docId: 'intro',
            position: 'left',
            label: 'Docs',
          },
          {
            href: `${jupyterhubUrl}`,
            label: 'JupyterHub Home',
            position: 'left',
          },
          {
            href: `${idrisDocsUrl}`,
            label: 'Jean Zay Docs',
            position: 'left',
          },
          { to: '/blog', label: 'Blog', position: 'left' },
          {
            href: `${repoUrl}`,
            label: 'GitLab',
            position: 'right',
          },
        ],
      },
      footer: {
        style: 'dark',
        links: [
          {
            title: 'Repository',
            items: [
              {
                label: 'Issues',
                href: `${repoUrl}/-/issues`,
              },
              {
                label: 'Merge Requests',
                href: `${repoUrl}/-/merge_requests`,
              },
            ],
          },
          {
            title: 'More',
            items: [
              {
                label: 'Blog',
                to: '/blog',
              },
              {
                label: 'GitLab',
                href: `${repoUrl}`,
              },
            ],
          },
        ],
        copyright: `Copyright © ${new Date().getFullYear()}. Built with Docusaurus.`,
      },
      prism: {
        theme: prismThemes.github,
        darkTheme: prismThemes.dracula,
      },
      algolia: {
        // The application ID provided by Algolia
        appId: 'MKN5FS5VH2',
        // Public API key: it is safe to commit it
        apiKey: '34987da5b8e464f88cb45457f4530f99',
        indexName: 'idris-cnrs-gitlab',
        // // Optional: Replace parts of the item URLs from Algolia. Useful when using the same search index for multiple deployments using a different baseUrl. You can use regexp or string in the `from` param. For example: localhost:3000 vs myCompany.com/docs
        replaceSearchResultPathname: {
          from: '/jupyter/jupyter-documentation/',
          to: '/',
        },
        // We may need to tune `contextualSearch` and `searchParameters` to handle search for versioned docs
        // Optional: see doc section -- https://docusaurus.io/docs/search#contextual-search
        contextualSearch: false,
        // Optional: Algolia search parameters
        // searchParameters: {},
        // Optional: path for search page that enabled by default (`false` to disable it)
        searchPagePath: 'search',
      },
    }),
  plugins: [
    [
      '@docusaurus/plugin-ideal-image',
      {
        quality: 90,
        max: 1030, // max size of redimensioned image.
        min: 600, // min size of redimensioned image.
        steps: 2, // number of images between max and min
        disableInDev: false,
      },
    ],
  ],
};

module.exports = config;
